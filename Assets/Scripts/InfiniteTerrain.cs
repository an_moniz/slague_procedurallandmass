﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteTerrain : MonoBehaviour
{
    private const float _scale = 1f;

    [SerializeField]private const float _viewerMoveThresholdForChunkUpdate = 25f;
    private const float _sqrViewerMoveThresholdForChunkUpdate = _viewerMoveThresholdForChunkUpdate * _viewerMoveThresholdForChunkUpdate;

    public LODInfo[] DetailLevels;
    public static float MaxViewDst = 450;

    public Transform Viewer;
    public Material MapMaterial;

    public static Vector2 ViewerPosition;
    private static Vector2 _viewerPositionOld;
    static MapGenerator _mapGenerator;
    private int _chunkSize;
    private int _chunksVisibleInViewDistance;

    private Dictionary<Vector2, TerrainChunk> _terrainChunkDictionary = new Dictionary<Vector2, TerrainChunk>();
    static List<TerrainChunk> _terrainChunksVisibleLastUpdate = new List<TerrainChunk>();

    private void Start()
    {
        _mapGenerator = FindObjectOfType<MapGenerator>();
        _chunkSize = MapGenerator.MapChunkSize - 1;
        _chunksVisibleInViewDistance = Mathf.RoundToInt( MaxViewDst / _chunkSize);

        MaxViewDst = DetailLevels[DetailLevels.Length - 1].VisibleDstThreshold;

        UpdateVisibleChunks();
    }

    private void Update()
    {
        ViewerPosition = new Vector2(Viewer.position.x, Viewer.position.z) / _scale;

        if ((_viewerPositionOld - ViewerPosition).sqrMagnitude > _sqrViewerMoveThresholdForChunkUpdate)
        {
            _viewerPositionOld = ViewerPosition;
            UpdateVisibleChunks();
        }
    }

    void UpdateVisibleChunks()
    {
        for (int i = 0, size = _terrainChunksVisibleLastUpdate.Count; i < size; i++)
        {
            _terrainChunksVisibleLastUpdate[i].SetVisible(false);
        }
        _terrainChunksVisibleLastUpdate.Clear();

        int currentChunkCoordX = Mathf.RoundToInt(ViewerPosition.x / _chunkSize);
        int currentChunkCoordY = Mathf.RoundToInt(ViewerPosition.y / _chunkSize);

        for (int yOffset = -_chunksVisibleInViewDistance; yOffset <= _chunksVisibleInViewDistance; yOffset++)
            for (int xOffset = -_chunksVisibleInViewDistance; xOffset <= _chunksVisibleInViewDistance; xOffset++)
            {
                Vector2 viewedChunkCoord = new Vector2(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);

                if (_terrainChunkDictionary.TryGetValue(viewedChunkCoord, out TerrainChunk terrainChunk))
                {
                    terrainChunk.Update();
                }
                else
                {
                    _terrainChunkDictionary.Add(viewedChunkCoord, new TerrainChunk(viewedChunkCoord, _chunkSize, DetailLevels, transform, MapMaterial));
                }
            }
    }
    public class TerrainChunk
    {
        GameObject _meshObject;
        Vector2 _position;
        Bounds _bounds;

        MeshRenderer _meshRenderer;
        MeshFilter _meshFilter;

        LODInfo[] _detailsLevels;
        LODMesh[] _lodMeshes;

        MapData _mapData;
        bool _mapDataReceived;
        int _previousLodIndex = -1;

        public TerrainChunk(Vector2 coord, int size, LODInfo[] detailsLevels, Transform parent, Material material)
        {
            _position = coord * size;
            _bounds = new Bounds(_position, Vector2.one * size);
            Vector3 positionV3 = new Vector3(_position.x, 0, _position.y);

            _meshObject = new GameObject("Terrain Chunk");
            _meshRenderer = _meshObject.AddComponent<MeshRenderer>();
            _meshFilter = _meshObject.AddComponent<MeshFilter>();
            _meshRenderer.material = material;
            _meshObject.transform.position = positionV3 * _scale;
            _meshObject.transform.parent = parent;
            _meshObject.transform.localScale = Vector3.one * _scale;

            _detailsLevels = detailsLevels;
            _lodMeshes = new LODMesh[_detailsLevels.Length];
            for (int i = 0; i < _detailsLevels.Length; i++)
            {
                _lodMeshes[i] = new LODMesh(_detailsLevels[i].Lod, Update);
            }

            SetVisible(false);

            _mapGenerator.RequestMapData(_position, OnMapDataReceived);
        }

        void OnMapDataReceived(MapData mapData)
        {
            _mapData = mapData;
            _mapDataReceived = true;

            Texture2D texture = TextureGenerator.TextureFromColorMap(mapData.ColorMap, MapGenerator.MapChunkSize, MapGenerator.MapChunkSize);
            _meshRenderer.material.mainTexture = texture;

            Update();
        }

        void OnMeshDataReceived(MeshData meshData)
        {
            _meshFilter.mesh = meshData.GenerateMesh();
        }

        public void Update()
        {
            if (!_mapDataReceived) return;
            // Find point on perimiter closest to viewer
            // Enable MeshObject if within Max View Distance
            float viewerDstFromNearestEdge = Mathf.Sqrt(_bounds.SqrDistance(ViewerPosition));
            bool visible = viewerDstFromNearestEdge <= MaxViewDst;

            if (visible)
            {
                int lodIndex = 0;
                for (int i = 0; i < _detailsLevels.Length - 1; i++) // last is already the MaxViewDst, which we've already tested
                {
                    if (viewerDstFromNearestEdge > _detailsLevels[i].VisibleDstThreshold)
                    {
                        lodIndex = i + 1;
                    }
                    else
                    {
                        break;
                    }
                }
                if (lodIndex != _previousLodIndex)
                {
                    LODMesh lodMesh = _lodMeshes[lodIndex];
                    if (lodMesh.HasMesh)
                    {
                        _previousLodIndex = lodIndex;
                        _meshFilter.mesh = lodMesh.Mesh;
                    }
                    else if (!lodMesh.HasRequestedMesh)
                    {
                        lodMesh.RequestMesh(_mapData);
                    }

                }
                _terrainChunksVisibleLastUpdate.Add(this);
            }
            SetVisible(visible);
        }
        public void SetVisible(bool visible)
        {
            _meshObject.SetActive(visible);
        }

        public bool IsVisible()
        {
            return _meshObject.activeSelf;
        }
    }

    /// <summary>
    /// Fetches own mesh from MapGenerator
    /// </summary>
    class LODMesh
    {
        public Mesh Mesh;
        public bool HasRequestedMesh;
        public bool HasMesh;
        private int _lod;

        System.Action _updateCallback;

        public LODMesh(int lod, System.Action updateCallback)
        {
            _lod = lod;
            Mesh = null;
            HasRequestedMesh = false;
            HasMesh = false;
            _updateCallback = updateCallback;
        }

        private void OnMeshDataReceived(MeshData meshData)
        {
            Mesh = meshData.GenerateMesh();
            HasMesh = true;
            _updateCallback();
        }

        public void RequestMesh(MapData mapData)
        {
            HasRequestedMesh = true;
            _mapGenerator.RequestMeshData(mapData, _lod, OnMeshDataReceived);
        }
    }

    [System.Serializable]
    public struct LODInfo
    {
        public int Lod;
        public float VisibleDstThreshold;
    }
}

