﻿using UnityEngine;

public static class MeshGenerator
{
    public static MeshData GenrateTerrainMesh(float[,] heightMap, float heightMultiplier, AnimationCurve heightCurve, int levelOfDetail)
    {
        AnimationCurve _heightCurve = new AnimationCurve(heightCurve.keys); // AnimationCurve can't be shared among threads, so we copy it in each thread

        int meshSimplifcationIncrement = (levelOfDetail == 0) ? 1 : levelOfDetail * 2;

        int borderedSize = heightMap.GetLength(0);
        int meshSize = borderedSize - 2 * meshSimplifcationIncrement;
        int meshSizeUnsimplified = borderedSize - 2;

        float topLeftX = (meshSize - 1) / -2f;
        float topLeftZ = (meshSize - 1) / 2f;

        int verticesPerLine = (meshSize - 1) / meshSimplifcationIncrement + 1;

        MeshData meshData = new MeshData(verticesPerLine);

        int[,] vertexIndicesMap = new int[borderedSize, borderedSize];
        int meshVertexIndex = 0;
        int borderVertexIndex = -1;

        for (int y = 0; y < borderedSize; y += meshSimplifcationIncrement)
        { 
            for (int x = 0; x < borderedSize; x += meshSimplifcationIncrement)
            {
                bool isBorderVertex = y == 0 || y == borderedSize - 1 ||
                    x == 0 || x == borderedSize - 1;
                if (isBorderVertex)
                {
                    vertexIndicesMap[x, y] = borderVertexIndex;
                    borderVertexIndex--;
                }
                else
                {
                    vertexIndicesMap[x, y] = meshVertexIndex;
                    meshVertexIndex++;
                }
            }
        }

        for (int y = 0; y < borderedSize; y += meshSimplifcationIncrement)
        {
            for (int x = 0; x < borderedSize; x += meshSimplifcationIncrement)
            {
                int vertexIndex = vertexIndicesMap[x, y];

                Vector2 percent = new Vector2((x - meshSimplifcationIncrement) / (float)meshSize, (y - meshSimplifcationIncrement) / (float)meshSize);
                float height = _heightCurve.Evaluate(heightMap[x, y]) * heightMultiplier;
                Vector3 vertexPosition = new Vector3(topLeftX + percent.x * meshSizeUnsimplified, height, topLeftZ - percent.y * meshSizeUnsimplified);

                meshData.AddVertex(vertexPosition, percent, vertexIndex);

                if (x < borderedSize - 1 && y < borderedSize - 1)
                {
                    int a = vertexIndicesMap[x, y];
                    int b = vertexIndicesMap[x + meshSimplifcationIncrement, y];
                    int c = vertexIndicesMap[x, y + meshSimplifcationIncrement];
                    int d = vertexIndicesMap[x + meshSimplifcationIncrement, y + meshSimplifcationIncrement];

                    meshData.AddTriangle(a, d, c);

                    meshData.AddTriangle(d, a, b);
                }

                vertexIndex++;
            }
        }
        return meshData;
    }
}

public class MeshData
{
    Vector3[] _vertices;
    int[] _triangles;
    Vector2[] _uvs;

    Vector3[] _borderVertices;
    int[] _borderTriangles;

    int _triangleIndex;
    int _borderTriangleIndex;
    public MeshData(int verticesPerLine)
    {
        _triangleIndex = 0;
        _vertices = new Vector3[verticesPerLine * verticesPerLine];
        _uvs = new Vector2[verticesPerLine * verticesPerLine];
        _triangles = new int[(verticesPerLine - 1) * (verticesPerLine - 1) * 2 * 3];

        _borderVertices = new Vector3[verticesPerLine * 4 + 4];// 4 sides, 4 corners
        _borderTriangles = new int[6 * 4 * verticesPerLine];
    }

    public void AddVertex(Vector3 vertexPosition, Vector2 uv, int vertexIndex)
    {
        if (vertexIndex < 0)
        {
            // border
            _borderVertices[-vertexIndex - 1] = vertexPosition;
        }
        else
        {
            _vertices[vertexIndex] = vertexPosition;
            _uvs[vertexIndex] = uv;
        }
    }

    public void AddTriangle(int a, int b, int c)
    {
        if (a < 0 || b < 0 || c < 0)
        {
            _borderTriangles[_borderTriangleIndex] = a;
            _borderTriangles[_borderTriangleIndex + 1] = b;
            _borderTriangles[_borderTriangleIndex + 2] = c;
            _borderTriangleIndex += 3;
        }
        else
        {
            _triangles[_triangleIndex] = a;
            _triangles[_triangleIndex + 1] = b;
            _triangles[_triangleIndex + 2] = c;
            _triangleIndex += 3;
        }
    }

    Vector3[] CalculateNormals()
    {
        Vector3[] vertexNormals = new Vector3[_vertices.Length];
        int triangleCount = _triangles.Length/3;

        for (int i = 0; i < triangleCount; i++)
        {
            int normalTriangleIndex = i * 3;
            int vertexIndexA = _triangles[normalTriangleIndex];
            int vertexIndexB= _triangles[normalTriangleIndex+1];
            int vertexIndexC = _triangles[normalTriangleIndex+2];

            Vector3 triangleNormal = SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);
            vertexNormals[vertexIndexA] = triangleNormal;
            vertexNormals[vertexIndexB] = triangleNormal;
            vertexNormals[vertexIndexC] = triangleNormal;
        }

        int borderTriangleCount = _borderTriangles.Length / 3;

        for (int i = 0; i < borderTriangleCount; i++)
        {
            int normalTriangleIndex = i * 3;
            int vertexIndexA = _borderTriangles[normalTriangleIndex];
            int vertexIndexB = _borderTriangles[normalTriangleIndex + 1];
            int vertexIndexC = _borderTriangles[normalTriangleIndex + 2];

            Vector3 triangleNormal = SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);
            if (vertexIndexA >= 0)
                vertexNormals[vertexIndexA] = triangleNormal;
            if (vertexIndexB >= 0)
                vertexNormals[vertexIndexB] = triangleNormal;
            if (vertexIndexC >= 0)
                vertexNormals[vertexIndexC] = triangleNormal;
        }

        return vertexNormals;
    }

    private Vector3 SurfaceNormalFromIndices(int iA, int iB, int iC)
    {
        Vector3 pointA = (iA < 0) ?  _borderVertices[-iA - 1] : _vertices[iA];
        Vector3 pointB = (iB < 0) ? _borderVertices[-iB - 1] : _vertices[iB];
        Vector3 pointC = (iC < 0) ? _borderVertices[-iC - 1] : _vertices[iC];

        Vector3 sideAB = pointB - pointA;
        Vector3 sideAC = pointC - pointA;
        return Vector3.Cross(sideAB, sideAC).normalized;
    }

    public Mesh GenerateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = _vertices;
        mesh.triangles = _triangles;
        mesh.uv = _uvs;
        //mesh.normals = CalculateNormals();
        mesh.RecalculateNormals();
        return mesh;
    }
}